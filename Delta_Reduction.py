cadena=''
def delta_reduction(application):
    global cadena
    while len(application) > 2:
        redundancy(application, 0)
        convert_app_cadena(application)
        #print ('cadena'+cadena)
    return cadena


def redundancy(application, index):
    if type(application[index + 1]) == int and type(application[index + 2]) == int:
        if application[index] == '+':
            aux = application[index + 1] + application[index + 2]
        elif application[index] == '-':
            aux = application[index + 1] - application[index + 2]
        elif application[index] == '*':
            aux = application[index + 1] * application[index + 2]
        else:
            aux = application[index + 1] / application[index + 2]

        if type(aux) == float:
            aux = round(aux, 2)

        application.pop(index)
        application.pop(index)
        application.pop(index)

        application.insert(index, aux)
        # view_ec(application)
    else:
        redundancy(application, index + 1)


def view_ec(app):
    print('->δ ', end='')
    for it in app:
        print(it, end=' ')

def convert_app_cadena(app):
    global cadena
    cadena=cadena+'\n->δ '
    for it in app:
        cadena=cadena+str(it)+' '
