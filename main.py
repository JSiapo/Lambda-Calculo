"""
Convert function to lambda calc
β reduction
δ reduction
"""

from ArgsAndApp import generate_argument_application
from Beta_Reduction import beta_convert
from Delta_Reduction import delta_reduction


def original_expression(expression):
    str_eq = ''
    for it in expression:
        str_eq = str_eq + it
    return str_eq


def view_expression(argument, application):
    str_eq=''
    if len(argument) != 0:
        str_eq=str_eq+'('
        for it in argument:
            str_eq=str_eq+'λ'+it
        str_eq=str_eq+'.'
    for it in application:
        str_eq=str_eq+it+' '
    if len(argument) != 0:
        str_eq=str_eq+')'
    return str_eq


def view_expression_beta(argument, application, values_argument):
    str_f=''
    if len(argument) != 0:
        for _ in values_argument:
            str_f=str_f+'('
        str_f=str_f+'('
        for it in argument:
            str_f=str_f+'λ'+it
        str_f=str_f+'.'
    for it in application:
        str_f=str_f+it+' '
    if len(argument) != 0:
        str_f=str_f+')'
        for it in values_argument:
            str_f=str_f+it+')'
    return str_f


def view_expression_with_values(argument, application, values_argument):
    str_exp=''
    for _ in values_argument:
        str_exp=str_exp+'('
    str_exp=str_exp+view_expression(argument, application)
    for it in values_argument:
        str_exp=str_exp+it+')'
    return str_exp


def view_beta_reduction(argument, application, values_argument,size_arguments):
    str_f=''
    for _ in range(0, size_arguments):
        str_f=str_f+'\n->β '
        (argument, application, values_argument) = beta_convert(argument, application, values_argument)
        str_f=str_f+'('+view_expression_beta(argument, application, values_argument)+')'
    return str_f


def convert(app):
    aux = []
    for index in app:
        if 47 < ord(index) < 58:
            aux.append(int(index))
        else:
            aux.append(index)
    return aux

def get_expression(eq):
    expression = []

    for i in eq:
        # delete with spaces
        if i != ' ':
            expression.append(i)
    (argument, application) = generate_argument_application(expression)
    size_arguments = len(argument)
    return view_expression(argument, application)

def get_values(vl):
    values_argument = []
    for i in vl:
        if i != ' ' and i!= ',':
            values_argument.append(i)
    return values_argument

def initMain(eq, vl):
    str_formula_lambda=''
    str_final=''
    expression = []

    for i in eq:
        # delete with spaces
        if i != ' ':
            expression.append(i)
    values_argument = []

    (argument, application) = generate_argument_application(expression)
    size_arguments = len(argument)

    for i in vl:
        if i != ' ' and i!= ',':
            values_argument.append(i)

    str_final = str_final+view_expression_with_values(argument, application,values_argument)+'\n'
    str_final = str_final+view_beta_reduction(argument, application,values_argument,size_arguments)+'\n'
    str_final_delta = delta_reduction(convert(application))
    return str_final,str_final_delta
