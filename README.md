# Lambda Calculo
*Curso: Especificación formal*

1. Convertir funcion a **lambda expression** separando argumento de aplicación (en expresión prefija)
2. **Beta reducción** (reemplazo de las variables por los valores agregados)
3. **Delta reducción** (resolver ecuación)

## Dependencias:

### Windows

```powershell
pip install PyQt5
```

### Linux

```bash
sudo apt install python3-pip
pip3 install PyQt5
```

## Ejecución

Activamos el entorno virtual

```bash
source venv/bin/activate
```

Ejecutamos el código

```python
python3 MainWindow.py
```

![Ejecucion](https://gitlab.com/JSiapo/Lambda-Calculo/raw/master/Screenshots/Ejecucion.png)

Cerramos el entorno virtual

```bash
deactivate
```

