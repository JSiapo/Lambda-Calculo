import sys

from ArgsAndApp import generate_argument_application
from Beta_Reduction import beta_convert
from Delta_Reduction import delta_reduction

from PyQt5.QtWidgets import QDialog, QApplication

from lambdac import Ui_Dialog
from main import initMain
from main import get_expression
from main import get_values

# expression for example is f(x+y+z)=(2*x+3)/(y-z*x)


class Windows(QDialog, Ui_Dialog):
    def __init__(self, parent=None):
        super(Windows, self).__init__(parent)
        self.setupUi(self)
        self.btn_ingresa_formula.clicked.connect(self.viewequation_labmda)
        self.btn_ingresa_valores.clicked.connect(self.view_values)
        self.pushButton_3.clicked.connect(self.view_calc)
        self.pushButton_4.clicked.connect(self.clear_text)

    def viewequation_labmda(self):
        equation = self.txt_formula.text()
        self.txt_formula_lambda.setText(get_expression(equation))

    def view_values(self):
        values = self.txt_valores.text()
        self.txt_valores_mostrar.setText(str(get_values(values)))

    def view_calc(self):
        (beta,delta)=initMain(self.txt_formula.text(),self.txt_valores.text())
        self.txt_beta.setText(beta)
        self.txt_delta.setText(delta)

    def clear_text(self):
        self.txt_beta.setText('')
        self.txt_delta.setText('')
        self.txt_formula.setText('')
        self.txt_valores.setText('')
        self.txt_formula_lambda.setText('')
        self.txt_valores_mostrar.setText('')

def convert(streq, strvalues):
    text=initMain(streq,strvalues)
    return text


def main():
    app = QApplication(sys.argv)
    windows = Windows()
    windows.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
